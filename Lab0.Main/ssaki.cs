﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    public class ssaki : zwierzeta
    {

        private string rodzaj_pokarmu;
        private int liczba_łap;
        private bool ogon;

        public ssaki(int wzrost, int waga, int wiek, string nazwa, string rodzaj_pokarmu, int liczba_łap, bool ogon)
            : base(wzrost, waga, wiek, nazwa)
        {
            this.rodzaj_pokarmu = rodzaj_pokarmu;
            this.liczba_łap = liczba_łap;
            this.ogon = ogon;
        }

        public ssaki() : base() { }

        public override string generetelines()
        {
            return base.generetelines() + this.liczba_łap + this.ogon;
        }
    }
}
